/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
const unsigned int PROTOTYPE_DELAYPERSTAGE = 500; // Delay in time per test stage. Change the total time in byte getEventduration() if you change this.


/**
 * Turn on every single light, open the gate and make various sounds. Reset everything afterwards.
 * This method will block input while it's testing.
 * You shouldn't be testing things when there are vehicles and pedestrians around in the first place.
 */
void testSystem()
{
	/* Turn everything on. */
	setLEDMatrix(8);
	setTrafficLight(3, true, true, true);
	setTrafficLight(4, true, true, true);
	setGate(true);

	/* Test the buzzer on frequencies 220, 330, 440, 550, 660, 770 and 880. */
	for (byte i = 2; i < 9; i++)
	{
		tone(PIN_BUZZER, 110 * i, PROTOTYPE_DELAYPERSTAGE);
		delay(PROTOTYPE_DELAYPERSTAGE);
	}

	/* Reset everything to default. */
	setLEDMatrix(0);
	setup_TrafficLight(); // Can't use its constants in this tab, and re-assigning pins won't hurt.
	setGate(false);

	delay(PROTOTYPE_DELAYPERSTAGE);

	currentEvent = 0; // Let system know event has ended.
}