/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
// See main tab: const byte PIN_BUZZER = 5;
const unsigned int BUZZER_FREQUENCY = 880;
const byte BUZZER_ON_TIME = 10;
const unsigned int BUZZER_SLOW_DELAY = 1000;
// See main tab: byte BUZZER_VERYFAST_DELAY = 100;
const byte BUZZER_FAST_DELAY = 200;


/* Declare variables. */
// See main tab: unsigned long buzzerTimer = 0;


/* Module-specific setup. */
void setup_Buzzer()
{
	pinMode(PIN_BUZZER, OUTPUT);
}


/**
 * Checks if it's time to sound the buzzer again and decides how long to wait before next button sound.
 */
void updateBuzzer()
{
	/* Depending on LEDMatrix state make specific sounds, for now every 5 seconds for testing. */
	if (millis() > buzzerTimer && (currentEvent != 2 || eventState < 3))
	{
		/* Set different values based off the current LED Matrix (crossing) state. */
		switch (LEDMatrix_stateID)
		{
		case 0: // Crossing closed, set the timer to play the slow ticking sound.
			buzzerTimer = millis() + BUZZER_SLOW_DELAY;
			break;

		case 7: // Crossing just opened, play the ticking sound very quickly.
			buzzerTimer = millis() + BUZZER_VERYFAST_DELAY;
			break;

		default: // All other values mean the crossing is open, set the timer to play 3 fast ticks every second.
			buzzerTimer = millis() + BUZZER_FAST_DELAY;
			eventState++; // eventState is put to 0 by void updateLEDMatrix() every second.
		}

		tone(PIN_BUZZER, BUZZER_FREQUENCY, BUZZER_ON_TIME); // Actually make the sound.
	}
}