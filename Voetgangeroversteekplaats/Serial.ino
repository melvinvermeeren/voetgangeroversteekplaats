/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
const unsigned long SERIAL_SPEED = 115200; // Faster = less program delay if it's using the serial port.
// const byte SERIAL_TYPE = SERIAL_8O1; // 8 data bits, uneven parity bit and 1 stop bit. This breaks serial port monitor input.


/* Module-specific setup. */
void setup_Serial()
{
	Serial.begin(SERIAL_SPEED); // Make it (SERIAL_SPEED, SERIAL_TYPE) to enable parity. This breaks serial port monitor input.
}


/**
 * Check for serial input and process it.
 */
void checkForSerialInput()
{
	if (Serial.available() > 0)
	{
		if (Serial.readString() == "log") // Valid command so send the log.
		{
			sendLogViaSerial();
		}
	}
}


/**
 * Send the log via serial.
 */
void sendLogViaSerial()
{
	for (unsigned int i = 0; i < LOG_SIZE; i++)
	{
		if (getLogInfoAtIndex(i) == "0") return; // Log ended here, so just return.

		/* Several print commands to save memory since adding Strings to each other creates a new string object. */
		Serial.print("#");
		Serial.print(String(i + 1));
		Serial.print(": ");
		Serial.println(getLogInfoAtIndex(i));
	}
}