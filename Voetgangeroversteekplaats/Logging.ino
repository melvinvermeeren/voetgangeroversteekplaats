/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
// See main tab: const byte LOG_SIZE = 30; // Slots in the log array, more means more history but also more memory usage. Max value 255 would take up at least 510 bytes.
const String LOG_MESSAGE_DURATIONBEFORE = " Duration: "; // Message placed before the duration of the event.
const String LOG_MESSAGE_DURATIONAFTER = " seconds."; // Message placed after the duration of the event.
const String LOG_MESSAGE_PLACEINQUEUEBEFORE = " Place in queue: "; // Message placed before the place in the queue.
const String LOG_MESSAGE_PLACEINQUEUEAFTER = "."; // Message placed after the place in the queue.
const String LOG_MESSAGES_INPUT[4] = // Array that contains all the various log messages for input.
{
	"System test requested.", // MessageID 1.
	"Pedestrian requestion to cross.", // MessageID 2.
	"Vehicle approaching left traffic light.", // Etcetera...
	"Vehicle approaching right traffic light."
};
const String LOG_MESSAGES_EVENT[4] = // Array that contains all the various log messages for events.
{
	"Testing system.",
	"Pedestrian crossing event started.",
	"Left traffic light event started.",
	"Right traffic light event started."
};
const String LOG_MESSAGES_SYSTEM[2] = // Array that contains all the various log messages for system.
{
	"Logging system initialized.",
	"Current event refreshed."
};


/* Declare variables. */
byte logArray[LOG_SIZE][2]; // Log array, first slot is index where [0] holds the newest entry, second slot is type where [0] = messageID and [1] = length.


/* Module-specific setup. */
void setup_Logging()
{
	addToLog(201, 0); // Add "Logging system initialized." message to log.
}


/**
 * Add an event to the log.
 * @param messageID.
 * @param event duration in seconds or place in queue.
 */
void addToLog(byte messageID, byte eventDurationPlaceInQueue)
{
	/* Shift all entries backwards, entries too old will be dropped out of array. */
	for (int i = LOG_SIZE - 1; i > 0; i--) // Minus 1 because it's an array.
	{
		if (logArray[i - 1][0] != 0) // Only shift if it's useful.
		{
			logArray[i][0] = logArray[i - 1][0];
			logArray[i][1] = logArray[i - 1][1];
		}
	}

	/* Add new entry to log. */
	logArray[0][0] = messageID;
	logArray[0][1] = eventDurationPlaceInQueue;
}


/**
 * Get log information at specified index.
 * @param index.
 * @return message with all information.
 */
String getLogInfoAtIndex(byte index)
{
	/* If specified index is empty return "0". */
	if (logArray[index][0] == 0) return "0";

	/* Else return the information. */
	if (logArray[index][0] <= 100) // IDs 1 through 100 are for input messages.
	{
		return LOG_MESSAGES_INPUT[logArray[index][0] - 1] + LOG_MESSAGE_PLACEINQUEUEBEFORE + logArray[index][1] + LOG_MESSAGE_PLACEINQUEUEAFTER;
	}
	else if (logArray[index][0] <= 200) // IDs 101 through 200 are for event messages.
	{
		return LOG_MESSAGES_EVENT[logArray[index][0] - 101] + LOG_MESSAGE_DURATIONBEFORE + logArray[index][1] + LOG_MESSAGE_DURATIONAFTER;
	}
	else // IDs 201 through 255 are for system messages.
	{
		return LOG_MESSAGES_SYSTEM[logArray[index][0] - 201];
	}
}