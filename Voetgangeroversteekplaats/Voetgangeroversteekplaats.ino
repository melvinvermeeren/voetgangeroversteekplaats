/**
 * �2013-2014 Melvin Vermeeren.
 * Application: Voetgangeroversteekplaats.
 * Initial release 10-Jan-2014.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 *
 * Made in Visual Studio 2013 with Visual Micro, CodeMaid and ReSharper.
 */


/* Constants from other tabs. (Because #include really doesn't work properly with Arduino, or not in a feasible way. */
const byte PIN_BUZZER = 5;
const byte BUZZER_VERYFAST_DELAY = 100;
const byte LEDMATRIX_NUMBEROFANIMATIONS = 8;
const byte LOG_SIZE = 30; // Slots in the log array, more means more history but also more memory usage. Max value 255 would take up at least 510 bytes.
const byte TRAFFICLIGHT_REFRESH_SETEVENTSTATE = 1; // If traffic light event in refreshed, set the eventState to this instead of to 0 (so that it goes back to green instead of red + yellow for Germany, etc).

/* Define constants. */
const byte EVENTLENGTH_PEDESTRIANCROSSING = 7;


/* Variables from other tabs. They are used at least once on another tab and this prevents writing a very simple method that returns the value. */
unsigned long buzzerTimer = 0;
byte LEDMatrix_stateID = 0; // 0 = cross; 1 = '1'; 2 = '2'; 3 = '3'; 4 = '4'; 5 = '5'; 6 = '6'; 7 = smiley.


/* Declare variables. */
byte eventState; // Variable to keep track of certain things in a state. 3 ticks per second for the crossing and current light status for the traffic lights.
byte currentEvent = 0; // Current event. 0 = All red; 1 = test system; 2 = pedestrian crossing; 3 = left traffic light; 4 = right traffic light.
unsigned long eventTimer = 0; // Is used to time the traffic lights and the LED matrix.
byte queue[4]; // 4 inputs so 4 slots in array.
byte queueLength = 0;


void setup()
{
	/* Module specific setups. */
	setup_Buzzer();
	setup_Gate();
	setup_Input();
	setup_LEDMatrix();
	setup_Logging();
	setup_Sensors();
	setup_Serial();
	setup_TrafficLight();
}


void loop()
{
	/* Check for input first. */
	checkForSerialInput();
	checkForButtonInput();

	/* Process queue if there is something to and if current event is all red OR the newest input in the same as the current state.
	   If the traffic light is currently active ignore the input only if the light is still in it's "going to green" phase, such as the red + yellow state of the German version.
	   In the last case the current state will be refreshed (e.g. stay on green for longer). */
	if (queueLength > 0) // Only check if there is something in queue.
	{
		if (((currentEvent == 3 && queue[0] == 3) || (currentEvent == 4 && queue[0] == 4)) && eventState <= TRAFFICLIGHT_REFRESH_SETEVENTSTATE - 1) shiftQueueForward();
		else if ((currentEvent == 0 || currentEvent == queue[0])) processQueue();
	}

	/* Update LED Matrix or traffic lights depending on event. */
	if (currentEvent == 2) updateLEDMatrix();
	else if (currentEvent == 3 || currentEvent == 4) updateTrafficLights();

	/* Always update the buzzer and gate. */
	updateBuzzer();
	updateGate();
}


/**
 * Add an inputType to queue, duplicate checking is done here also.
 * It will also add the input to the log.
 * @param inputType. (0 = Pedestrian crossing; 1 = Left traffic light; 2 = Right traffic light; 255 = Test system button.
 */
void addToQueue(byte inputType)
{
	if (!isInputInQueue(inputType)) // Only add if inputType isn't in queue yet.
	{
		queue[queueLength] = inputType; // Add input to queue. +1 not needed since array starts at 0.

		queueLength++; // Increase queue length.

		addToLog(inputType, queueLength); // Add input to log.
	}
}


/**
 * Shift the queue one forward (drop "old" next in line).
 */
void shiftQueueForward()
{
	/* Shift positions. */
	queue[0] = queue[1];
	queue[1] = queue[2];
	queue[2] = queue[3];
	queue[3] = NULL; // Reset queue[3] to prevent future shifting issues.

	queueLength--; // Decrease queue length.
}


/**
 * Checks if this input type is already in the queue.
 * @param inputType.
 * @return true or false.
 */
bool isInputInQueue(byte inputType)
{
	/* If it already exists return true. */
	for (int i = 0; i < queueLength; i++)
	{
		if (queue[i] == inputType) return true;
	}

	/* Else return false. */
	return false;
}


/**
 * Process the next item in the queue and shift queue up afterwards.
 */
void processQueue()
{
	/* Check if it's refreshing a state instead of changes and add a special message to the log for this. */
	bool addEventToLog = true;
	if (currentEvent == queue[0])
	{
		addEventToLog = false;
		addToLog(202, getEventDuration());
	}

	/* Update variables. */
	if ((currentEvent == 3 || currentEvent == 4) && currentEvent == queue[0] && eventState > TRAFFICLIGHT_REFRESH_SETEVENTSTATE - 1) eventState = TRAFFICLIGHT_REFRESH_SETEVENTSTATE - 1; // See description of TRAFFICLIGHT_REFRESH_SETEVENTSTATE.
	else eventState = 0; // Reset eventState.
	eventTimer = 0; // Instantly start the animation.
	currentEvent = queue[0]; // Update currentEvent.

	/* Process first slot in queue. */
	switch (queue[0])
	{
	case 1: // Test system.
		addToLog(101, getEventDuration());
		testSystem();
		break;

	case 2: // Pedestrian crossing.
		if (addEventToLog) addToLog(102, getEventDuration());
		LEDMatrix_stateID = LEDMATRIX_NUMBEROFANIMATIONS; // Set LEDMatrix stateID to smiley.
		if (buzzerTimer > millis() + BUZZER_VERYFAST_DELAY) buzzerTimer = 0; // Force update buzzer if it would else wait longer than the very fast delay time.
		break;

	case 3: // Left traffic light.
	case 4: // Right traffic light.
		if (addEventToLog) addToLog(100 + currentEvent, getEventDuration());
		break;

	default: // Undefined queue ID, break and shift queue and reset queue variables.
		currentEvent = 0; // All red.
		break;
	}

	shiftQueueForward(); // Shift queue up.
}


/**
 * Get duration in seconds of the current event.
 * @return current event duration in seconds.
 */
byte getEventDuration()
{
	switch (currentEvent)
	{
	case 1: // Test system.
		return 4;

	case 2: // Pedestrian crossing.
		return EVENTLENGTH_PEDESTRIANCROSSING;

	case 3: // Left traffic light.
	case 4: // Right traffic light.
		return getTrafficLightTotalDuration();

	default: // Undefined ID, return 0.
		return 0;
	}
}