/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
const byte PIN_BUTTON_LEFTTRAFFICLIGHT = 4;
const byte PIN_BUTTON_RIGHTTRAFFICLIGHT = 2;
const byte PIN_BUTTON_PEDESTRIANCROSSING = 3;
const byte PIN_BUTTON_TESTSYSTEM = 7;
const byte BUTTONTIMER_DELAY = 250;


/* Declare variables. */
unsigned long buttonTimer = 0; // Prevents buttons from registering very quickly, which sort of ruins the log.


/* Module-specific setup. */
void setup_Input()
{
	pinMode(PIN_BUTTON_LEFTTRAFFICLIGHT, INPUT);
	pinMode(PIN_BUTTON_RIGHTTRAFFICLIGHT, INPUT);
	pinMode(PIN_BUTTON_PEDESTRIANCROSSING, INPUT);
	pinMode(PIN_BUTTON_TESTSYSTEM, INPUT);
}


/**
 * Check for button input and process it.
 */
void checkForButtonInput()
{
	if (millis() > buttonTimer)
	{
		if (digitalRead(PIN_BUTTON_TESTSYSTEM) == HIGH) // System test button.
		{
			addToQueue(1);
			buttonTimer = millis() + BUTTONTIMER_DELAY;
		}

		if (digitalRead(PIN_BUTTON_PEDESTRIANCROSSING) == HIGH) // Pedestrian crossing button.
		{
			addToQueue(2);
			buttonTimer = millis() + BUTTONTIMER_DELAY;
		}

		if (digitalRead(PIN_BUTTON_LEFTTRAFFICLIGHT) == HIGH) // Left traffic light button.
		{
			addToQueue(3);
			buttonTimer = millis() + BUTTONTIMER_DELAY;
		}

		if (digitalRead(PIN_BUTTON_RIGHTTRAFFICLIGHT) == HIGH) // Right traffic light button.
		{
			addToQueue(4);
			buttonTimer = millis() + BUTTONTIMER_DELAY;
		}
	}
}