/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Includes. */
#include <Wire.h>


/* Define constants. */
const unsigned int LEDMATRIX_DELAY_PER_STATE = 1000; // How long each second of the countdown takes in milliseconds.
const byte I2C_LEDMATRIX_ADRES = 0x70;
const byte LEDMATRIX_SIZE = 8;
// See main tab: const byte LEDMATRIX_NUMBEROFANIMATIONS = 8;
const byte LEDMATRIX_ANIMATIONS[9][8] = // LED Animations array. Note that they are mirrored vertically in order to be displayed properly due to display orientation.
{
	{
		/* stateID 0: cross. */
		B10000001,
		B01000010,
		B00100100,
		B00011000,
		B00011000,
		B00100100,
		B01000010,
		B10000001
	},
	{
		/* stateID 1: '1'. This font and the other digits are taken from font_8x8-1.h found at https://www.schoology.com/page/80168781 (lesson 6.3). */
		B00011000,
		B00111000,
		B01111000,
		B00011000,
		B00011000,
		B00011000,
		B00011000,
		B01111110
	},
	{
		/* stateID 2: '2'. */
		B00111100,
		B01000010,
		B00000010,
		B00000100,
		B00001000,
		B00010000,
		B00100000,
		B01111110
	},
	{
		/* stateID 3: '3'. */
		B00000000,
		B00111100,
		B01000010,
		B00000010,
		B00011100,
		B00000010,
		B01000010,
		B00111100
	},
	{
		/* stateID 4: '4'. */
		B00001000,
		B00011000,
		B00101000,
		B01001000,
		B01111100,
		B00001000,
		B00001000,
		B00001000
	},
	{
		/* stateID 5: '5'. */
		B01111110,
		B01000000,
		B01000000,
		B01111100,
		B00000010,
		B00000010,
		B01000010,
		B00111100
	},
	{
		/* stateID 6: '6'. */
		B00111110,
		B01000000,
		B01000000,
		B01111100,
		B01000010,
		B01000010,
		B01000010,
		B00111100
	},
	{
		/* stateID 7: smiley. */
		B00111100,
		B01000010,
		B10100101,
		B10000001,
		B10100101,
		B10011001,
		B01000010,
		B00111100
	},
	{
		/* stateID 8: all LEDs (for void testSystem() only). */
		B11111111,
		B11111111,
		B11111111,
		B11111111,
		B11111111,
		B11111111,
		B11111111,
		B11111111
	}
};


/* Declare variables. */
// See main tab: byte LEDMatrix_stateID = 0; // 0 = cross; 1 = '1'; 2 = '2'; 3 = '3'; 4 = '4'; 5 = '5'; 6 = '6'; 7 = smiley.


/* Module-specific setup. */
void setup_LEDMatrix()
{
	Wire.begin();

	Wire.beginTransmission(I2C_LEDMATRIX_ADRES);  // Initiate display.
	Wire.write(0x21);
	Wire.endTransmission();

	Wire.beginTransmission(I2C_LEDMATRIX_ADRES);  // Setup blinking, turn it off.
	Wire.write(0x81);
	Wire.endTransmission();

	Wire.beginTransmission(I2C_LEDMATRIX_ADRES);  // Setup brightness, set it to max.
	Wire.write(0xE0);
	Wire.endTransmission();

	setLEDMatrix(LEDMatrix_stateID); // Write default position to LED matrix.
}


/**
 * Update the LED Matrix.
 */
void updateLEDMatrix()
{
	if (LEDMatrix_stateID > 0 && millis() > eventTimer)
	{
		eventTimer = millis() + LEDMATRIX_DELAY_PER_STATE; // Reset timer.

		LEDMatrix_stateID--; // Count down the state.

		setLEDMatrix(LEDMatrix_stateID); // And actually update the display itself.

		eventState = 0; // Reset eventState so the buzzer will make 3 ticks every second properly.

		if (LEDMatrix_stateID == 0) currentEvent = 0; // If the event has let the system know by setting this to 0 (all red) again.
	}
}


/**
 * Set the LED Matrix to a new state.
 * @param stateID. 0 through 7: cross, '1', '2', '3', '4', '5', '6', smiley.
 */
void setLEDMatrix(byte stateID)
{
	Wire.beginTransmission(I2C_LEDMATRIX_ADRES); // Start transmission.
	Wire.write(0x00); // Write first 0x00 pair.

	for (byte i = LEDMATRIX_SIZE; i > 0; i--) fixAndWriteToLEDMatrix(LEDMATRIX_ANIMATIONS[stateID][i - 1]); // Write new LED matrix state. This also flips vertically.

	Wire.endTransmission(); // End transmission.
}


/**
 * Fixes the LED Matrix bug and writes the line to the matrix, also sends the 0x00 message afterwards.
 * @param byte containing new LED states.
 */
void fixAndWriteToLEDMatrix(byte input)
{
	Wire.write(input >> 1 | input << 7);
	Wire.write(0x00);
}