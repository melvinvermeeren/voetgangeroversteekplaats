/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
const byte PIN_LEFTTRAFFICLIGHT_GREEN = 13;
const byte PIN_LEFTTRAFFICLIGHT_YELLOW = 12;
const byte PIN_LEFTTRAFFICLIGHT_RED = 11;
const byte PIN_RIGHTTRAFFICLIGHT_GREEN = 8;
const byte PIN_RIGHTTRAFFICLIGHT_YELLOW = 9;
const byte PIN_RIGHTTRAFFICLIGHT_RED = 10;
const unsigned int TRAFFICLIGHT_YELLOWTIME_MINFACTOR = 1000; // Factor is divided by 1000, so 1000 actually is factor 1.0.
const unsigned int TRAFFICLIGHT_YELLOWTIME_MAXFACTOR = 3000; // These are used to map the factor for yellow time based of sensors.
// See main tab: const byte TRAFFICLIGHT_REFRESH_SETEVENTSTATE = 1; // If traffic light event in refreshed, set the eventState to this instead of to 0 (so that it goes to green instead of red + yellow for Germany, etc).
const byte TRAFFICLIGHT_YELLOWSTATE = 2; // Stage where the traffic light is at yellow since the time on yellow needs to be modified depending in temperature.
const byte TRAFFICLIGHT_NUMBEROFSTAGES = 3; // Change the byte in getTrafficLightTotalDuration() to int if this reaches 255.
const bool TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES][3] = // Order is red, yellow and green left to right and order of stages downwards.
{
	//{ true, true, false }, // Germany and Austria.
	{ false, false, true },
	//{ false, false, false }, // Austria
	//{ false, false, true },	// Austria.
	//{ false, false, false }, // Austria.
	//{ false, false, true },	// Austria.
	{ false, true, false },
	{ true, false, false }
};
const unsigned int TRAFFICLIGHT_STAGES_DELAY[TRAFFICLIGHT_NUMBEROFSTAGES] = // Amount of milliseconds to wait per stage. Note that time on yellow is modified by sensor input.
{
	//1000, // Germany and Austria.
	7000,
	//250, // Austria.
	//250, // Austria.
	//250, // Austria.
	//250, // Austria.
	2000,
	2000 // Time on red is basically how much delay AFTER setting light to red before checking queue.
};


/* Module-specific setup. */
void setup_TrafficLight()
{
	pinMode(PIN_LEFTTRAFFICLIGHT_GREEN, OUTPUT);
	pinMode(PIN_LEFTTRAFFICLIGHT_YELLOW, OUTPUT);
	pinMode(PIN_LEFTTRAFFICLIGHT_RED, OUTPUT);
	pinMode(PIN_RIGHTTRAFFICLIGHT_GREEN, OUTPUT);
	pinMode(PIN_RIGHTTRAFFICLIGHT_YELLOW, OUTPUT);
	pinMode(PIN_RIGHTTRAFFICLIGHT_RED, OUTPUT);

	/* Write end state to both traffic lights. */
	setTrafficLight(3, TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][0], TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][1],
		TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][2]);
	setTrafficLight(4, TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][0], TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][1],
		TRAFFICLIGHT_STAGES[TRAFFICLIGHT_NUMBEROFSTAGES - 1][2]);
}


/**
 * Update the traffic lights.
 */
void updateTrafficLights()
{
	if (millis() > eventTimer)
	{
		if (eventState == TRAFFICLIGHT_NUMBEROFSTAGES) // Final stage ended, let the system know by setting currentEvent to 0. and return afterwards.
		{
			currentEvent = 0;
		}
		else
		{
			/* If it's time for yellow modify yellow time based of sensor values. Else set it normally. */
			if (eventState == TRAFFICLIGHT_YELLOWSTATE - 1)
			{
				eventTimer = millis() + int((TRAFFICLIGHT_STAGES_DELAY[eventState] *
					(map(getHighestSensorValue(), 0, 1023, TRAFFICLIGHT_YELLOWTIME_MINFACTOR, TRAFFICLIGHT_YELLOWTIME_MAXFACTOR) / 1000)));
			}
			else eventTimer = millis() + TRAFFICLIGHT_STAGES_DELAY[eventState];

			/* Update traffic light. */
			setTrafficLight(currentEvent, TRAFFICLIGHT_STAGES[eventState][0], TRAFFICLIGHT_STAGES[eventState][1], TRAFFICLIGHT_STAGES[eventState][2]);

			eventState++; // Update eventState.
		}
	}
}


/**
 * Set the specified traffic light's lights to a specified state.
 * @param trafficLightID. 3 = Left traffic light; 4 = right traffic light.
 * @param New state of red light.
 * @param New state of yellow light.
 * @param New state of green light.
 */
void setTrafficLight(byte trafficLightID, bool redLight, bool yellowLight, bool greenLight)
{
	if (trafficLightID == 3) // Left traffic light.
	{
		digitalWrite(PIN_LEFTTRAFFICLIGHT_RED, redLight);
		digitalWrite(PIN_LEFTTRAFFICLIGHT_YELLOW, yellowLight);
		digitalWrite(PIN_LEFTTRAFFICLIGHT_GREEN, greenLight);
	}
	else // Right traffic light.
	{
		digitalWrite(PIN_RIGHTTRAFFICLIGHT_RED, redLight);
		digitalWrite(PIN_RIGHTTRAFFICLIGHT_YELLOW, yellowLight);
		digitalWrite(PIN_RIGHTTRAFFICLIGHT_GREEN, greenLight);
	}
}


/**
 * Returns the amount of seconds an entire traffic light loop will take.
 * @return amount of seconds entire loop will take.
 */
byte getTrafficLightTotalDuration()
{
	unsigned int totalDuration = 0;

	for (byte i = 0; i < TRAFFICLIGHT_NUMBEROFSTAGES; i++)
	{
		totalDuration += TRAFFICLIGHT_STAGES_DELAY[i];
	}

	return totalDuration / 1000;
}