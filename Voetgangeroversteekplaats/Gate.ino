/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Includes. */
#include <Servo.h>
Servo gate; // Create Servo object.


/* Define constants. */
const byte PIN_GATE = 6;
const byte GATE_OPEN_ANGLE = 30;
const byte GATE_CLOSED_ANGLE = 125;


/* Module-specific setup. */
void setup_Gate()
{
	gate.attach(PIN_GATE); // Attach servo to pin.
}


/**
 * Set the gate open or closed.
 * @param new state. true = open; false = closed.
 */
void setGate(bool newState)
{
	if (newState) gate.write(GATE_OPEN_ANGLE);
	else gate.write(GATE_CLOSED_ANGLE);
}


/**
 * Update the gate depending on LED Matrix state.
 */
void updateGate()
{
	if (LEDMatrix_stateID == 0) setGate(false);
	else setGate(true);
}