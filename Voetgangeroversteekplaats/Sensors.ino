/**
 * �2013-2014 Melvin Vermeeren.
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 */


/* Define constants. */
const byte PIN_LDR = A0;
const byte PIN_THERMISTOR = A1;
/* analogRead() gets a value of 0 min and 1023 max, those are also the limits of the following 4 constants.
   However, more realistic values for the LDR may be between 200 and 800. So map 200 trough 800 into 0 through 1023.
   That was a reading of perhaps 700 would give a value of around 900 (just a guess, may be less or more).
   At a end result of 0 the yellow time is modified max, as end result goes up yellow time is more like the base time.
   See tab TrafficLight for more information. */
const unsigned int LDR_MIN_READING = 0; // Very dark.
const unsigned int LDR_MAX_READING = 800; // 50W very bright desktop light.
const unsigned int THERMISTOR_MIN_READING = 200; // Just a guess.
const unsigned int THERMISTOR_MAX_READING = 600; // ~23 degrees Celsius.
const bool LDR_INVERT = false; // It should give low values when it's dark, if not set this to true.
const bool THERMISTOR_INVERT = false; // It should give low values when it's cold, if not set this to true.


/* Module-specific setup. */
void setup_Sensors()
{
	pinMode(PIN_LDR, INPUT);
	pinMode(PIN_THERMISTOR, INPUT);
}


/**
 * Gets the highest sensor value and translates it and returns it.
 * @return highest sensor value, range is 0 - 1023.
 */
unsigned int getHighestSensorValue()
{
	unsigned int LDRValue;
	unsigned int thermistorValue;

	if (!LDR_INVERT) LDRValue = constrain(map(analogRead(PIN_LDR), LDR_MAX_READING, LDR_MIN_READING, 0, 1023), 0, 1023);
	else LDRValue = constrain(map(analogRead(PIN_LDR), LDR_MIN_READING, LDR_MAX_READING, 0, 1023), 0, 1023);

	if (!THERMISTOR_INVERT) thermistorValue = constrain(map(analogRead(PIN_THERMISTOR), THERMISTOR_MAX_READING, THERMISTOR_MIN_READING, 0, 1023), 0, 1023);
	else thermistorValue = constrain(map(analogRead(PIN_THERMISTOR), THERMISTOR_MIN_READING, THERMISTOR_MAX_READING, 0, 1023), 0, 1023);

	if (LDRValue > thermistorValue) return LDRValue;
	return thermistorValue;
}